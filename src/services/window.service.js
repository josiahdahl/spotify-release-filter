let activeWindow = null;

export const openPage = (url, focus = false) => {
  if (!url) {
    throw new Error('Please pass a URL');
  }
  if (activeWindow) {
    activeWindow.location = url;
  } else {
    activeWindow = window.open(url);
  }
  if (focus) {
    activeWindow.focus();
  }
} ;