import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { spotifyClient } from '../services/spotify';
import { Redirect, Route } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => {
  spotifyClient.checkAuth();
  return <Route
    {...rest}
    render={props => spotifyClient.isAuthorized
      ? (<Component {...props}/>)
      : (<Redirect to={{ pathname: '/login', state: { from: props.location } }}/>)
    }/>
};

PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.instanceOf(Component),
  ]).isRequired,
};