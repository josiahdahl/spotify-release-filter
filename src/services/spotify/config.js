export const CLIENT_ID = '07fbb51728d34bfd8af057fb27b5cd5d';
export const AUTHORIZE_ENDPOINT = 'https://accounts.spotify.com/authorize';
export const REDIRECT_URL = process.env.REACT_APP_SPOTIFY_AUTH_CALLBACK
    ? process.env.REACT_APP_SPOTIFY_AUTH_CALLBACK
    : 'http://localhost:3000/auth-callback';
export const BASE_URL = 'https://api.spotify.com/v1';
export const LOGIN_URL = '/login';