import React from 'react';
import { Button, Card as SemanticUICard, Icon, Image } from 'semantic-ui-react';
import { SpotifyAlbumProps } from './index';

export const Card = ({ artists, id, images, name, onOpen, external_urls }) => {
  return (
    <SemanticUICard>
      <Image src={images[0].url}/>
      <SemanticUICard.Content>
        <SemanticUICard.Header>{name}</SemanticUICard.Header>
        <SemanticUICard.Meta>{artists.map(a => a.name).join(', ')}</SemanticUICard.Meta>
      </SemanticUICard.Content>
      <SemanticUICard.Content extra>
        <Button basic icon labelPostion='left' fluid onClick={() => onOpen(external_urls.spotify)}>
          <Icon name='spotify'/>
          Open in Spotify
        </Button>
      </SemanticUICard.Content>
    </SemanticUICard>
  );
};

Card.propTypes = SpotifyAlbumProps;