import PropTypes from 'prop-types';
import React from 'react';
import { Button, Icon } from 'semantic-ui-react';

export const Pagination = ({title, numItems, itemsPerPage, currentPage, onPrev, onNext}) => {
  const pages = Math.ceil(numItems / itemsPerPage);
  const pageString = `${currentPage} of ${pages}`;
  return (
    <React.Fragment>
      {title !== undefined ? <p style={{ fontWeight: 'bold' }}>{title}</p> : ''}
      <Button.Group style={{border: '1px solid #eee'}}>
        <Button basic icon disabled={currentPage === 1} onClick={onPrev}><Icon name='chevron left'/></Button>
        <Button basic>{pageString}</Button>
        <Button basic icon disabled={currentPage === pages} onClick={onNext}><Icon name='chevron right'/></Button>
      </Button.Group>
    </React.Fragment>
  );
};

Pagination.propTypes = {
  title: PropTypes.string,
  numItems: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPrev: PropTypes.func.isRequired,
  onNext: PropTypes.func.isRequired,
};