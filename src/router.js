import React from 'react';
import { HashRouter } from 'react-router-dom';
import { Provider } from 'rebass';
import { App } from './App';

export const Router = () =>
  <HashRouter>
    <Provider>
      <App/>
    </Provider>
  </HashRouter>;