import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { spotifyClient } from '../services/spotify';

export class AuthCallbackPage extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor() {
    super();
    spotifyClient.emitAuthorizationData();
    window.close();
  }

  render() {
    return (
      <div>Authorizing</div>
    );
  }
}