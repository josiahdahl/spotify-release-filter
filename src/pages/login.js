import React, { Component } from 'react';
import styled from 'styled-components';
import { Redirect } from 'react-router-dom';
import { spotifyClient } from '../services/spotify';
import { AuthorizeForm } from '../components/AuthorizeForm';

const FullHeightFlex = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

export class LoginPage extends Component {

  state = {
    redirectToReferrer: false,
    error: null,
  };

  constructor() {
    super();
    this.handleAuthorize = this.handleAuthorize.bind(this);
    this.handleAuthorizeFail = this.handleAuthorizeFail.bind(this);
  }

  handleAuthorize() {
    this.setState({ redirectToReferrer: true });
  }

  handleAuthorizeFail() {
    this.setState({
      redirectToReferrer: false,
      errorMessage: 'Authorization failed',
    });
  }

  render() {
    const { redirectToReferrer, errorMessage } = this.state;
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    return (
      redirectToReferrer || spotifyClient.isAuthorized
        ? <Redirect to={from}/>
        : <FullHeightFlex justifyContent='center' alignItems='center'>
          <AuthorizeForm errorMessage={errorMessage} onAuthorize={this.handleAuthorize} onAuthorizeFail={this.handleAuthorizeFail}/>
        </FullHeightFlex>
    );
  }
}