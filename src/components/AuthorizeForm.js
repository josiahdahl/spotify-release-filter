import PropTypes from 'prop-types';
import React from 'react';
import { Button, Card } from 'semantic-ui-react';
import styled from 'styled-components';
import { spotifyClient } from '../services/spotify';

const authorize = (resolve, reject) => {
  return spotifyClient.requestAuthorization()
    .then(resolve, reject);
};

const Error = styled.p`
  color: #f00;
  font-weight: bold;
`;

export const AuthorizeForm = ({ errorMessage, onAuthorize, onAuthorizeFail }) => {
  return (
    <Card>
      <Card.Content>
        <Card.Header textAlign='center'>Login with Spotify</Card.Header>
      </Card.Content>
      <Card.Content textAlign='center'>
        <Button primary onClick={() => authorize(onAuthorize, onAuthorizeFail)}>Authorize</Button>
        {errorMessage ? <Error>{errorMessage}</Error> : ''}
      </Card.Content>
    </Card>
  );
};

AuthorizeForm.propTypes = {
  errorMessage: PropTypes.string,
  onAuthorize: PropTypes.func.isRequired,
  onAuthorizeFail: PropTypes.func.isRequired,
};