import styled from 'styled-components';
import React from 'react';

export const FlagImage = styled.img.attrs({
  src: props => props.src,
  alt: props => props.alt || '',
})`
display: block;
margin-right: 0.5em;
width: 70px;
height: 70px;
flex-grow: 0;
flex-shrink: 0;
`;

export const FlagBody = ({ children }) => <div>{children}</div>;

const UnstyledFlag = ({ className, children }) => <div className={className}>{children}</div>;

export const Flag = styled(UnstyledFlag)`
display: flex;`;

