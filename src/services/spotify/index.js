import Axios from 'axios';

import { CLIENT_ID, AUTHORIZE_ENDPOINT, REDIRECT_URL, BASE_URL, LOGIN_URL } from './config';

class SpotifyClient {
  clientId;
  authorizeEndpoint;
  redirectUrl;
  baseURL;
  loginURL;
  axiosClient;

  requestState;
  token;
  expires;

  user;

  scopes = ['user-read-playback-state', 'user-read-currently-playing', 'user-modify-playback-state', 'streaming'];

  constructor(clientId, authorizeEndpoint, redirectUrl, baseURL, loginURL) {
    this.clientId = clientId;
    this.authorizeEndpoint = authorizeEndpoint;
    this.redirectUrl = redirectUrl;
    this.baseURL = baseURL;
    this.loginURL = loginURL;
  }

  createAxiosClient() {
    this.axiosClient = Axios.create({
      baseURL: this.baseURL,
      ...this.headers,
    });
    this.axiosClient.interceptors.response.use(response => response, (err) => {
      const { status } = err;
      if (status === 401) {
        return this.requestAuthorization()
          .then(() => this.axiosClient(err.config));
      }
    });
  }

  get isAuthorized() {
    const now = Date.now();
    return this.token && this.expires > now;
  }

  get headers() {
    return {
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    };
  }

  checkAuth() {
    this.expires = localStorage.getItem('spotify_expires_in');
    this.token = localStorage.getItem('spotify_access_token');
    if (!this.axiosClient) {
      this.createAxiosClient();
    }
  }

  requestAuthorization() {
    return new Promise((resolve, reject) => {
      // http://localhost:3000/auth-callback#access_token=BQD-ry9Lfhao8IG7jijVrPHXPDBCBT809hu1mm4IrCbc1-UOQ43OWlkDRegfmsEuFkfdXykOdwo9bv412Ohho0QyhYML4rEOytXhpC70zUrL6bK2V3CZZ0dZARQIEYcVNBPXL_431rTOp5fG&token_type=Bearer&expires_in=3600&state=1529532365337
      this.requestState = Date.now();
      const authUrl = `${this.authorizeEndpoint}?client_id=${this.clientId}&response_type=token&redirect_uri=${encodeURIComponent(this.redirectUrl)}&state=${this.requestState}&scope=${encodeURIComponent(this.scopes.join(' '))}`;
      localStorage.setItem('spotify_request_state', this.requestState);
      const width = 450;
      const height = 730;
      /* eslint-disable */
      const left = (screen.width / 2) - (width / 2);
      const top = (screen.height / 2) - (height / 2);
      /* eslint-enable */

      window.addEventListener("message", ({ data }) => {
        if (typeof data === 'string' && data.indexOf('access_token') !== -1) {
          /** @type {{access_token: string, token_type: 'Bearer', expires_in: string, state: string}}*/
          const parsedData = data
            .replace(/^#/, '')
            .split('&')
            .reduce((acc, kv) => {
              const [key, value] = kv.split('=');
              return Object.assign({}, acc, { [key]: value });
            }, {});

          if (this.validStateToken(parsedData.state)) {
            this.addAuthData(parsedData);
            this.createAxiosClient();
            resolve();
          } else {
            reject();
          }
        }
      }, false);

      window.open(authUrl,
        'Spotify',
        'menubar=no,location=no,resizable=no,scrollbars=no,status=no,width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
      );
    });

  }

  emitAuthorizationData() {
    const hashData = window.location.hash;
    window.opener.postMessage(hashData, '*');
  }

  /**
   * @param data {{access_token: string, token_type: 'Bearer', expires_in: string, state: string}}
   */
  addAuthData(data) {
    this.token = data.access_token;
    this.expires = Date.now() + parseInt(data.expires_in, 10) * 1000;
    localStorage.setItem('spotify_expires_in', this.expires);
    localStorage.setItem('spotify_access_token', this.token);
    localStorage.removeItem('spotify_request_state');
  }

  validStateToken(stateToken) {
    const currentState = this.requestState || localStorage.getItem('spotify_request_state');

    return parseInt(stateToken, 10) === parseInt(currentState, 10);
  }

  loadUser() {
    return this.axiosClient.get('/me');
  }

  loadNewReleases(offset = 0, country = 'US') {
    return this.axiosClient.get(`/browse/new-releases?country=${country}&offset=${offset}`);
  }
}

export const spotifyClient = new SpotifyClient(CLIENT_ID, AUTHORIZE_ENDPOINT, REDIRECT_URL, BASE_URL, LOGIN_URL);



