import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from 'styled-components';
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'rebass';
import { App } from './App';


injectGlobal`
* { box-sizing: border-box; }
html { min-height: 100%; height: 100%; }
body { 
  margin: 0;
  min-height: 100%;
  height: 100%;
}
`;

ReactDOM.render(
  <BrowserRouter>
    <Provider>
      <App/>
    </Provider>
  </BrowserRouter>
  , document.getElementById('root')
);
registerServiceWorker();
