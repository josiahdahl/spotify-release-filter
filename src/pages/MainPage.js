import React, { Component } from 'react';
import { Button, Checkbox, Grid, Transition } from 'semantic-ui-react';
import _chunk from 'lodash/chunk';
import { spotifyClient } from '../services/spotify';
import { SpotifyAlbumCompactCard, SpotifyAlbumCard } from '../components/SpotifyAlbum';
import { Pagination } from '../components/Pagination';
import { openPage } from '../services/window.service';

export class MainPage extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {
    pagination: {
      itemsPerPage: 20,
      currentOffset: 0,
    },
    user: {},
    releases: {
      entities: {},
      ids: [],
      offset: null,
      total: null,
      previous: null,
    },
    filter: null,
    layout: 'normal',
  };

  constructor() {
    super();
    // this.handleOpen = this.handleOpen.bind(this);
    // this.handlePrev = this.handlePrev.bind(this);
    // this.handleNext = this.handleNext.bind(this);
    spotifyClient.loadUser()
      .then(({ data }) => {
        this.setState({ user: data });
      });
    const { currentOffset } = this.state.pagination;
    this.loadReleases(currentOffset);


  }

  handleOpen(url) {
    openPage(url, true);
  }

  handlePrev() {
    const { currentOffset } = this.state.pagination;
    const previousPage = currentOffset - 1;
    const setState = () => this.setState(state => ({
      pagination: {
        ...state.pagination,
        currentOffset: previousPage,
      },
    }));
    spotifyClient.loadNewReleases(previousPage)
      .then(setState);

  }

  handleNext() {
    const { currentOffset } = this.state.pagination;
    const nextPage = currentOffset + 1;
    this.loadReleases(nextPage)
      .then(() => {
        this.setState(state => ({
          ...state,
          pagination: {
            ...state.pagination,
            currentOffset: nextPage,
          },
        }));
      });
  }

  setFilter(filter) {
    this.setState({ filter });
  }

  setLayout() {
    const newLayout = this.state.layout === 'normal' ? 'compact' : 'normal';

    this.setState({
      layout: newLayout,
    });
  }

  loadReleases(offset) {
    const { itemsPerPage } = this.state.pagination;
    return spotifyClient.loadNewReleases(offset * itemsPerPage)
      .then(({ data }) => {
        const { items, offset, total, previous } = data.albums;
        const currentIds = this.state.releases.ids;
        const { entities, ids } = items.reduce((newItems, item) => {
          if (currentIds.findIndex(item => item.id) === -1) {
            newItems = {
              entities: { ...newItems.entities, [item.id]: item },
              ids: [...newItems.ids, item.id],
            };
          }
          return newItems;
        }, { entities: {}, ids: [] });

        this.setState({
          releases: {
            entities: {
              ...this.state.releases.entities,
              ...entities,
            },
            ids: [...this.state.releases.ids, ...ids],
            offset,
            total,
            previous,
          }
        });
        return Promise.resolve();
      });
  }

  render() {
    const { releases, filter, pagination, layout } = this.state;
    const { entities, ids, total } = releases;
    const { currentOffset, itemsPerPage } = pagination;

    const items = _chunk(ids, itemsPerPage)[currentOffset] || [];
    const filteredItems = items.map(id => entities[id])
      .filter(item => filter === null ? true : item.album_type === filter);
    return (
      <React.Fragment>
        <div style={{ display: 'flex', padding: '1em' }}>
          <div>
            <p style={{ fontWeight: 'bold' }}>Filter</p>
            <Button.Group color='blue'>
              <Button onClick={() => this.setFilter(null)}>All</Button>
              <Button onClick={() => this.setFilter('album')}>Albums</Button>
              <Button onClick={() => this.setFilter('single')}>Singles</Button>
            </Button.Group>
          </div>
          <div style={{ marginLeft: '1em' }}>
            <Pagination
              title='Pages'
              currentPage={currentOffset + 1}
              itemsPerPage={itemsPerPage}
              numItems={total}
              onNext={() => this.handleNext()}
              onPrev={() => this.handlePrev()}
            />
          </div>
          <div style={{ display: 'flex', padding: '1em', flexDirection: 'column' }}>
            <p style={{ fontWeight: 'bold' }}>Layout</p>
            <div style={{ display: 'flex', width: '175px', alignItems: 'center', justifyContent: 'space-between' }}>
              <div>Normal</div>
              <Checkbox toggle checked={layout === 'compact'} onChange={() => this.setLayout()}/>
              <div>Compact</div>
            </div>
          </div>
        </div>
        <Transition.Group as={Grid} padded animation='browse' duration={300}>
          {filteredItems.map(album => (
              <div key={album.id} style={{ display: 'flex', justifyContent: 'center', minWidth: '250px', minHeight: layout === 'normal' ? '440px' : '180px'}}>
                {layout === 'normal'
                  ? <SpotifyAlbumCard {...album} onOpen={url => this.handleOpen(url)}/>
                  : <SpotifyAlbumCompactCard onOpen={url => this.handleOpen(url)} {...album}/>
                }
              </div>
            )
          )}
        </Transition.Group>
      </React.Fragment>
    );
  }
}