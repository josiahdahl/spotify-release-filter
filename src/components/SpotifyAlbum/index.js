import PropTypes from 'prop-types';

export const SpotifyAlbumProps = {
  onOpen: PropTypes.func.isRequired,
  artists: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
      type: PropTypes.string,
    }).isRequired
  ),
  id: PropTypes.string.isRequired,
  images: PropTypes.arrayOf(
    PropTypes.shape({
      height: PropTypes.number,
      width: PropTypes.number,
      url: PropTypes.string,
    }).isRequired
  ),
  name: PropTypes.string.isRequired,
  external_urls: PropTypes.shape({
    spotify: PropTypes.string,
  }).isRequired,
};

export { Card as SpotifyAlbumCard } from './Card';
export { CompactCard as SpotifyAlbumCompactCard } from './CompactCard';