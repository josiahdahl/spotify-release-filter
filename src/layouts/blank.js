import PropTypes from 'prop-types';
import React from 'react';

export const BlankLayout = ({ children = '' }) => {
  return (
    <div>
      {children}
    </div>
  );
};

BlankLayout.propTypes = {};