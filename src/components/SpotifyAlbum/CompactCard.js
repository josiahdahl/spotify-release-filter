import React from 'react';
import { Button, Card as SemanticUICard, Icon } from 'semantic-ui-react';
import { SpotifyAlbumProps } from './index';
import { Flag, FlagBody, FlagImage } from '../Flag';

export const CompactCard = ({ artists, id, images, name, onOpen, external_urls }) => {
  return (
    <SemanticUICard>
      <SemanticUICard.Content>
        <Flag>
          <FlagImage src={images[0].url}/>
          <FlagBody>
            <SemanticUICard.Header>{name}</SemanticUICard.Header>
            <SemanticUICard.Meta>{artists.map(a => a.name).join(', ')}</SemanticUICard.Meta>
          </FlagBody>
        </Flag>
      </SemanticUICard.Content>
      <SemanticUICard.Content extra>
        <Button basic icon labelPostion='left' fluid onClick={() => onOpen(external_urls.spotify)}>
          <Icon name='spotify'/>
          Open in Spotify
        </Button>
      </SemanticUICard.Content>
    </SemanticUICard>
  );
};

CompactCard.propTypes = SpotifyAlbumProps;