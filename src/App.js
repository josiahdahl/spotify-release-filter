import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { LoginPage } from './pages/login';
import { MainPage } from './pages/MainPage';
import { PrivateRoute } from './containers/private-route';
import { AuthCallbackPage } from './pages/AuthCallbackPage';
import { spotifyClient } from './services/spotify';

export class App extends Component {
  constructor() {
    super();
    spotifyClient.checkAuth();
  }
  render() {
    return (
      <Switch>
        <PrivateRoute exact path='/' component={MainPage}/>
        <Route path='/login' component={LoginPage}/>
        <Route path='/auth-callback' component={AuthCallbackPage}/>
      </Switch>
    );
  }
}
